![NuProj.Thumbnail.png](https://bitbucket.org/repo/774B7G/images/1907898025-NuProj.Thumbnail.png)

# NuGet.PackOnBuild

Builds a NuGet-package on a successful build in _Release_-configuration, and puts the output to _solution-dir\NuGet-packed_

## Installation

Via [NuGet](https://www.nuget.org/packages/gfoidl.NuGet.PackOnBuild) :smiley: