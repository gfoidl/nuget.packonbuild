﻿using System.Diagnostics.Contracts;

namespace gfoidl.NuGet.PackOnBuild.Test
{
	public class Mathe
	{
		public int Addiere(int a, int b)
		{
			return a + b;
		}
		//---------------------------------------------------------------------
		public int Dividiere(int a, int b)
		{
			Contract.Requires(b != 0);

			return a / b;
		}
	}
}