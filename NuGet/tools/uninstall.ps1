param($installPath, $toolsPath, $package, $project)

$name = $project.Name + ".nuspec"

If (Test-Path $name) {
	$specFile = $project.ProjectItems.Item($name)
	#$specFile.Name = "default.nuspec-Template"         w�re auch m�glich, da es dann von NuGet entfernt wird -> ich entferne es aber gleich direkt
	$specFile.Delete()
}